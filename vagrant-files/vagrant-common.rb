# -*- mode: ruby -*-
# vi: set ft=ruby :

# Global functions
# ----------------
  # Functions to ask some parameters
  class Username
    def to_s
      print "Need your ASTTRACE Login/Password.\n"
      print "Login: "
      STDIN.gets.chomp
    end
  end
  class Password
    # encoding: US_ASCII
    def to_s
      begin
      system 'stty -echo'
      print "Password: "
      pass = STDIN.gets.chomp
      #print pass
      ensure
      system 'stty echo'
      end
      pass.encode("ASCII")
    end
  end

# Global variables
# ----------------
MSG_BEGIN_P = "\n===============================================================================\n*******************************************************************************\n[VAGRANT-PROVISION] "
MSG_BEGIN = "\n===============================================================================\n*******************************************************************************\n"
MSG_END = "\n*******************************************************************************\n===============================================================================\n"

# Move folder .vagrant in other space to not be shared it as nfs folder
# ----------------
VAGRANT_DOTFILE_PATH = "../#{VAGRANT_SUBDIR}.vagrant__CACHE__";
currpath = ENV['VAGRANT_DOTFILE_PATH'];
if(currpath.nil?)
    currpath = '.vagrant';
end
puts currpath #debugging
if(currpath != VAGRANT_DOTFILE_PATH)
    ENV['VAGRANT_DOTFILE_PATH'] = VAGRANT_DOTFILE_PATH
    ret = system "vagrant",*ARGV
    FileUtils.rm_r(currpath)
    if(ret)
      exit
    else
      abort "Finished"
    end
end

# PLUGINS INSTALLATION
# --------------------
# List plugins dependencies to install automaticaly
plugin_status = false
VAGRANT_PLUGINS_DEPENDENCIES.each do |plugin_name|
  unless Vagrant.has_plugin? plugin_name
    system("vagrant plugin install #{plugin_name}")
    plugin_status = true
    puts " #{plugin_name}  Dependencies installed"
  end
end
# Restart Vagrant if any new plugin installed
if plugin_status === true
  exec "vagrant #{ARGV.join' '}"
else
  puts "All Plugin Dependencies already installed"
end

