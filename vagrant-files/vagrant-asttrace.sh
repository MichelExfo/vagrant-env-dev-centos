# .bashrc

echo "\n=====> Install autofs packages <=====\n"
# autofs, cifs-utils
yum install -y autofs cifs-utils
sh -c 'cat <<EOF_CAT > /etc/auto.autofs
asttrace -fstype=cifs,ro,credentials=/etc/auto.cifs.credentials ://10.50.50.148/traces
EOF_CAT'
sh -c 'cat <<EOF_CAT > /etc/auto.cifs.credentials
username=$ASTTRACE_LOGIN
domain=astellia.loc
password=$ASTTRACE_PWD
EOF_CAT'
sh -c 'cat <<EOF_CAT >> /etc/auto.master
/autofs     /etc/auto.autofs     --ghost
EOF_CAT'
systemctl enable autofs && systemctl start autofs
echo "DONE\n"

