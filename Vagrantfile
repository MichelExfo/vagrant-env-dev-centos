# -*- mode: ruby -*-
# vi: set ft=ruby :

# Global variables
# ----------------
VAGRANT_FILES = "vagrant-files"
VAGRANT_DIR = File.dirname(__FILE__)
VAGRANT_SUBDIR = File.basename(Dir.getwd)
VAGRANT_HOST_ID = "CentOS77"
VAGRANT_LOCAL_IP_ADDR = '192.168.16.70'
VAGRANT_PLUGINS_DEPENDENCIES = %w(vagrant-vbguest vagrant-winnfsd vagrant-bindfs vagrant-hostmanager vagrant-docker-compose)

require "./#{VAGRANT_FILES}/vagrant-common.rb"

# PROVISIONNINGS
# --------------
$provisioning_ast = <<-PROVISIONING_AST
echo "#{MSG_BEGIN_P} Using repos ASTELLIA #{MSG_END}"

# Repo BETA 7.5
yum install -y http://beta.repo.domaintests.loc/repo/Astellia_Repo_CentOS_7.5/astellia/yum-ast-repo-7.5.1.6-el7.noarch.rpm
/opt/astellia/tools/ast_repo.py --install=beta.repo.domaintests.loc
yum clean all
yum update
# Adding external repos (/!\ To do for developpment only!!!)
sudo cat <<EOF_CAT >> /etc/yum.repos.d/astellia.repo
[Astellia_Repo_CentOS_7_All]
name= Astellia_Repo_CentOS_7 Private 
baseurl=http://10.35.0.253/mrepo_private/CENTOS7-x86_64/RPMS.all 
enabled=1 
gpgcheck=0 
EOF_CAT

# Installing tools
sudo yum -y install exfo-platform-docker 

# Open Port after PSS installed
sudo yum install -y astellia_pss
echo Update sshd_config and iptables for vagrant access
sudo sed -i "s/^\\(AllowGroups.*\\)/# \\1/" /etc/ssh/sshd_config
sudo sed -i "s/^\\(Banner.*\\)/# \\1/" /etc/ssh/sshd_config
sudo sed -i "s/^#Port 22/Port 22/" /etc/ssh/sshd_config
sudo sed -i "s;PasswordAuthentication no;PasswordAuthentication yes;" /etc/ssh/sshd_config
sudo sed -i "s/^PermitRootLogin no/PermitRootLogin yes/" /etc/ssh/sshd_config
sudo iptables -I INPUT -p tcp --dport 22 -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 2377 -j ACCEPT
sudo iptables-save
sudo service sshd restart

PROVISIONING_AST

$provisioning_base = <<-PROVISIONING_BASE
echo "#{MSG_BEGIN_P} Create repository to save all data (shared or not) #{MSG_END}"
mkdir -p /home/vagrant/sourcecode
chown vagrant:vagrant /home/vagrant/sourcecode
mkdir -p /home/vagrant/dockers
chown vagrant:vagrant /home/vagrant/dockers
sudo cat <<EOF_CAT >> /home/vagrant/dockers/.env
LOCAL_IP_ADDR=#{VAGRANT_LOCAL_IP_ADDR}
EOF_CAT

echo "#{MSG_BEGIN_P} Upgrade CentOS packages #{MSG_END}"
#sudo yum update -y
sudo yum clean all && sudo rm -rf /var/cache/yum
echo "DONE\n"

echo "#{MSG_BEGIN_P} Install personnal packages #{MSG_END}"
sudo yum -y install xauth vim-enhanced mlocate nmap dos2unix tree tcpdump net-tools lsof
sudo updatedb
echo "DONE\n"

echo "#{MSG_BEGIN_P} Remove SSH Identity File authentification #{MSG_END}"
sudo sed -i "s;PasswordAuthentication no;PasswordAuthentication yes;" /etc/ssh/sshd_config
sudo sed -i "s/^PermitRootLogin no/PermitRootLogin yes/" /etc/ssh/sshd_config
sudo service sshd restart
echo "DONE\n"

PROVISIONING_BASE

$provisioning_dev = <<-PROVISIONING_DEV
echo "#{MSG_BEGIN_P} Install Development Tools #{MSG_END}"
#sudo yum -y groupinstall "Development tools"
sudo yum -y install autoconf automake binutils bison cmake flex gcc gcc-c++ gettext libtool make patch pkgconfig redhat-rpm-config rpm-build rpm-sign strace git subversion
echo "DONE\n"

echo "#{MSG_BEGIN_P} Install other development packages #{MSG_END}"
# git 2.24.0 (sources https://tecadmin.net/install-git-on-centos-fedora)
sudo yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc perl-ExtUtils-MakeMaker
wget https://www.kernel.org/pub/software/scm/git/git-2.24.0.tar.gz
tar xzf git-2.24.0.tar.gz && cd git-2.24.0
sudo make prefix=/usr/local/git all
sudo make prefix=/usr/local/git install
cd .. && sudo rm -rf git-2.24.0*
sudo sh -c 'echo "export PATH=/usr/local/git/bin:$PATH" >> /etc/bashrc'
# svn 1.13.0 (sources http://www.linuxfromscratch.org/blfs/view/svn/general/subversion.html)
wget https://archive.apache.org/dist/apr/apr-1.7.0.tar.bz2
tar xvf apr-1.7.0.tar.bz2 && cd apr-1.7.0
./configure --prefix=/usr --disable-static --with-installbuilddir=/usr/share/apr-1/build && make
sudo make install
cd .. && sudo rm -rf apr-1.7.0*
sudo yum -y install expat-devel
wget https://archive.apache.org/dist/apr/apr-util-1.6.1.tar.bz2
tar xvf apr-util-1.6.1.tar.bz2 && cd apr-util-1.6.1
./configure --prefix=/usr --with-apr=/usr --with-gdbm=/usr --with-openssl=/usr --with-crypto && make
sudo make install
cd .. && sudo rm -rf apr-util-1.6.1*
wget https://sqlite.org/2019/sqlite-autoconf-3300100.tar.gz
tar xvf sqlite-autoconf-3300100.tar.gz && cd sqlite-autoconf-3300100
./configure --prefix=/usr --disable-static --enable-fts5 CFLAGS="-g -O2 -DSQLITE_ENABLE_FTS3=1 -DSQLITE_ENABLE_FTS4=1 -DSQLITE_ENABLE_COLUMN_METADATA=1 -DSQLITE_ENABLE_UNLOCK_NOTIFY=1 -DSQLITE_ENABLE_DBSTAT_VTAB=1 -DSQLITE_SECURE_DELETE=1 -DSQLITE_ENABLE_FTS3_TOKENIZER=1" && make
sudo make install
cd .. && sudo rm -rf sqlite-autoconf-3300100*
wget https://archive.apache.org/dist/subversion/subversion-1.13.0.tar.bz2
tar xvf subversion-1.13.0.tar.bz2 && cd subversion-1.13.0
./configure --prefix=/usr --disable-static --with-apache-libexecdir --with-lz4=internal --with-utf8proc=internal && make
sudo make install && sudo install -v -m755 -d /usr/share/doc/subversion-1.13.0 && sudo cp -v -R doc/* /usr/share/doc/subversion-1.13.0
cd .. && sudo rm -rf subversion-1.13.0*
# python 2
curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py
sudo python get-pip.py
rm -f get-pip.py
sudo pip install virtualenv virtualenvwrapper
# python 3
sudo yum -y install https://centos7.iuscommunity.org/ius-release.rpm
sudo yum update
sudo yum -y install python36u  python36u-libs python36u-devel python36u-pip
sudo ln -sf /usr/bin/python3.6 /usr/bin/python3
sudo ln -sf /usr/bin/pip3.6 /usr/bin/pip3
# /!\ TO USE IF provisioning_ast IS USE !!!
#sudo yum -y install python36 python36-pip python36-devel
# colout
git clone https://github.com/nojhan/colout.git
cd colout && git checkout v0.6.1 && sudo python3 setup.py install
cd .. && sudo rm -rf colout
# robotframework (more information here: http://planet.astellia.loc/jcms/p_156341/fr/environment-for-creating-automatic-tests-with-robotframework)
sudo pip install robotframework
sudo pip install robotframework-csvlibrary
sudo pip install robotframework-difflibrary
sudo pip install robotframework-sshlibrary
sudo pip install suds
# cppcheck
sudo yum -y install pcre-devel
git clone https://github.com/danmar/cppcheck.git
cd cppcheck && git checkout 1.89 && make MATCHCOMPILER=yes FILESDIR=/usr/share/cppcheck HAVE_RULES=yes && sudo make install FILESDIR=/usr/share/cppcheck
cd .. && rm -rf cppcheck
#sudo yum install cppcheck
# lcov + tools use by it
git clone https://github.com/linux-test-project/lcov.git
cd lcov && git checkout v1.14 && sudo make install
cd .. && rm -rf lcov
sudo yum -y install perl-Digest-MD5
sudo yum -y install perl-PerlIO-gzip
sudo yum -y install perl-JSON
# sloccount
git clone https://git.code.sf.net/p/sloccount/code sloccount-code
cd sloccount-code && sudo make install
cd .. && rm -rf sloccount-code
echo "DONE\n"

echo "#{MSG_BEGIN_P} Intall extra package #{MSG_END}"
sudo yum -y install valgrind kcachegrind graphviz htop xclip bindfs
echo "DONE\n"

echo "#{MSG_BEGIN_P} Visual Source Code customization #{MSG_END}"
ln -sf /home/vagrant/sourcecode/host/#{VAGRANT_FILES}/.vscode /home/vagrant/sourcecode/.vscode
#ln -sf /home/vagrant/sourcecode/host/#{VAGRANT_FILES}/vscode-aqp.code-workspace /home/vagrant/sourcecode/vscode-aqp.code-workspace
#ln -sf /home/vagrant/sourcecode/host/#{VAGRANT_FILES}/vscode-controller.code-workspace /home/vagrant/sourcecode/vscode-controller.code-workspace
#ln -sf /home/vagrant/sourcecode/host/#{VAGRANT_FILES}/vscode-kafka_consumer.code-workspace /home/vagrant/sourcecode/vscode-kafka_consumer.code-workspace
#ln -sf /home/vagrant/sourcecode/host/#{VAGRANT_FILES}/vscode-processing_pipeline.code-workspace /home/vagrant/sourcecode/vscode-processing_pipeline.code-workspace
#ln -sf /home/vagrant/sourcecode/host/#{VAGRANT_FILES}/vscode-scalableup_post_treat.code-workspace /home/vagrant/sourcecode/vscode-scalableup_post_treat.code-workspace
sudo sh -c 'cat <<EOF_CAT >> /etc/sysctl.conf
fs.inotify.max_user_watches=524288
EOF_CAT'
sudo sysctl -p
echo "DONE\n"

echo "#{MSG_BEGIN_P} Intall gcc 8, gdb 8, ... : to launch it under virtual environment --> 'scl enable devtoolset-8 bash' #{MSG_END}"
sudo yum -y install centos-release-scl
sudo yum -y install devtoolset-8
sudo yum -y install cmake3
sudo ln -sf /usr/bin/cmake3 /opt/rh/devtoolset-8/root/bin/cmake
echo "DONE\n"

PROVISIONING_DEV

$provisioning_dev_aqp = <<-PROVISIONING_DEV_AQP
# For AQP
sudo yum install -y libpcap-devel doxygen protobuf-c-devel openssl-devel cyrus-sasl-devel
echo "DONE\n"

PROVISIONING_DEV_AQP

$provisioning_dev_flex = <<-PROVISIONING_DEV_FLEX
# For Flex / Capture_Unit
sudo yum install -y bzip2 numactl-devel numactl kernel-devel libpcap-devel libpcap net-snmp net-snmp-devel cyrus-sasl-devel protobuf-c-devel
echo "DONE\n"

PROVISIONING_DEV_FLEX

# FINAL MESSAGE TO DISPLAY
# ------------------------
$postupmessage_ast = <<-POSTUPMESSAGE_AST
#{MSG_BEGIN} 
All DONE? 
 --> Search inside this cmd window the world 'No package'. You must not find this text!
SSH CONNECTION: (with astellia image)
 --> ssh vagrant@#{VAGRANT_LOCAL_IP_ADDR} -p 22222
 --> ssh vagrant@127.0.0.1
#{MSG_END}
 
POSTUPMESSAGE_AST

$postupmessage = <<-POSTUPMESSAGE
#{MSG_BEGIN} 
All DONE? 
 --> Search inside this cmd window the world 'No package'. You must not find this text!
SSH CONNECTION: (with external image)
 --> ssh vagrant@#{VAGRANT_LOCAL_IP_ADDR} -i "#{VAGRANT_DIR}\\#{VAGRANT_DOTFILE_PATH}\\machines\\default\\virtualbox\\private_key"
 or
 --> ssh vagrant@127.0.0.1
VSCODE REMOTE CONNECTION: 
 --> in case of connection error, delete line #{VAGRANT_LOCAL_IP_ADDR} inside file "C:\\Users\\m.legruley\\.ssh\\known_hosts"
VSCODE COMPILATION WITH MAKE: 
 --> in case of SVN update error, check if you have on your host machine a windows tortoise open. Close it to solve this problem
GRAFANA CONNECTION: (supervision)
 --> http://#{VAGRANT_LOCAL_IP_ADDR}:3000
PORTAINER CONNECTION: (docker supervision)
--> http://#{VAGRANT_LOCAL_IP_ADDR}:9000
REGISTER CONNECTION: (docker register)
--> http://#{VAGRANT_LOCAL_IP_ADDR}:5000/v2
#{MSG_END}
 
POSTUPMESSAGE

# VM CONFIGURATION
# ----------------
Vagrant.configure("2") do |config|
 
  # Clean up folder on the host after the guest is destroyed
  config.trigger.after :destroy do |trigger|
    trigger.info = "#{MSG_BEGIN} Destroy DONE.\n You can now delete folder: rm -r #{VAGRANT_DOTFILE_PATH} #{MSG_END}"
  end
  config.trigger.before :provision do |trigger|
    trigger.info = "#{MSG_BEGIN} BE CAREFUL: Login and Password will be ask you during provisioning (to be connected to ASTTRACE)! #{MSG_END}"
  end

  # /!\ TO USE IF provisioning_ast IS USE !!!
  #config.vm.box = 'exfo/centos-7.5'
  #config.vm.box_download_insecure = true
  #config.vm.box_url = 'https://nexus.astellia.loc/content/sites/packer/centos-7.5.virtualbox.box'
  config.vm.box = 'centos/7'
  config.vm.box_version = "1905.1"
  config.vm.box_check_update = false
  config.vm.hostname = "#{VAGRANT_HOST_ID}.local"
  # /!\ TO USE IF provisioning_ast IS USE !!!
  #config.vm.post_up_message = $postupmessage_ast
  config.vm.post_up_message = $postupmessage
  
  # Provider-specific configuration
  config.vm.provider "virtualbox" do |vb|
    vb.name = "#{VAGRANT_HOST_ID}-#{rand(01..99)}"
    vb.memory = 4096                                        # Customize the amount of memory
    vb.cpus = 2                                             # Customize the amount of cpus
    vb.customize ["modifyvm", :id, "--vram", "128"]         # Set the video memory to 128Mb
    vb.customize ["modifyvm", :id, "--ioapic", "on"]        # Enable more than 1 CPU per VM
    vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"] # Enable symlinks on shared folders
    #vb.gui = true
  end

  # Provisioning (be carefull, '"shell", env:' option is only functionnal with 'path:' and not 'inline:' parameter !!!)
  config.vm.provision "shell", env: {"ASTTRACE_LOGIN" => Username.new, "ASTTRACE_PWD" => Password.new}, path: "#{VAGRANT_FILES}/vagrant-asttrace.sh"
  config.vm.provision "file", source: "#{VAGRANT_FILES}/vagrant-bashrc", destination: ".bashrc"
  config.vm.provision "shell", inline: "cp /home/vagrant/.bashrc /root/.bashrc"
  config.vm.provision "shell", inline: "rm /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime", run: "always"
  # /!\ TO USE IF provisioning_ast IS USE !!!
  #config.vm.provision "shell", inline: $provisioning_ast, privileged: false
  config.vm.provision "shell", inline: $provisioning_base, privileged: false
  config.vm.provision "shell", inline: $provisioning_dev, privileged: false
  config.vm.provision "shell", inline: $provisioning_dev_aqp, privileged: false
  #config.vm.provision "shell", inline: $provisionning_dev_flex, privileged: false
  # Set docker + Grafana
  config.vm.provision 'shell', inline: "git clone https://github.com/stefanprodan/dockprom dockers/dockprom"
  config.vm.provision :docker
  config.vm.provision :docker_compose, env: { ADMIN_USER: "admin", ADMIN_PASSWORD: "admin" }, yml: ["/home/vagrant/dockers/dockprom/docker-compose.yml"], run: "always"
  config.vm.provision "file", source: "#{VAGRANT_FILES}/vagrant-docker-compose.yml", destination: "dockers/vagrant-docker-compose.yml"
  config.vm.provision :docker_compose, yml: ["/home/vagrant/dockers/vagrant-docker-compose.yml"], run: "always"
  # Set to known IdentityFile to not use '-i' option with ssh connection
  #ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
  #config.vm.provision 'shell', inline: 'mkdir -p /root/.ssh'
  #config.vm.provision 'shell', inline: "echo #{ssh_pub_key} >> /root/.ssh/authorized_keys"
  #config.vm.provision 'shell', inline: "echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys", privileged: false

  # Share an additional folder to the guest VM: First argument = host path / Second argument = guest path
  # --> Share once at creation with (vagrant up) or at restart (vagrant reload)
  #config.vm.synced_folder ".", "/vagrant"
  # --> Share always in both side with winnfsd plugin + bindfs to manage owner properties
  config.vm.synced_folder '.', '/vagrant', disabled: true
  if (Vagrant.has_plugin?('vagrant-bindfs') && Vagrant.has_plugin?('vagrant-winnfsd'))
    config.vm.synced_folder "./", "/mnt/tmp_bindfs", type: 'nfs', create: true
    config.bindfs.bind_folder "/mnt/tmp_bindfs", "/home/vagrant/sourcecode/host", after: :provision, perms: "u=rwx:g=rwx:o=rx"
  end
  
  # /!\ TO USE IF provisioning_ast IS USE !!!
  #config.vm.network :forwarded_port, host: 22, guest: 22222, id: "ssh", auto_correct: true
  config.vm.network :forwarded_port, host: 22, guest: 22, id: "ssh", auto_correct: true
  # Create a private network, which allows host-only access to the machine
  config.vm.network "private_network", ip: "#{VAGRANT_LOCAL_IP_ADDR}"

  # Plugins to manage all hosts (not important)
  if Vagrant.has_plugin?("vagrant-vm-manager-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
  end
end