# Saclable UP Solution

## Introduction

It contains environment to create vagrant + vscode environment.

## Prerequisites

Vagrant and VirtualBox installed.

## Usage

Create/Launch: vagrant up

Stop: vagrant halt

Delete: vagrant destroy

## More informations

* [Link to Vagrant](https://www.vagrantup.com)
